# High Fidelity Placename Redirector #

This is a small script that checks if a user has the right placename when entering an area, if its not the correct one then it will redirec them to this location.

### Features ###

* Easy way to redirect people to the correct place if you have multiple on the same domain.

### How to use it ###

You can use this [localhost example](https://bitbucket.org/thecollectivesl/place-name-redirector/raw/master/box-localhost-example.json) to look at how it works. However just adding placeName value with your placename as shown below.

![userdata](https://bitbucket.org/thecollectivesl/place-name-redirector/raw/master/img/userdata.png)

you can put the following code into your objects userdata to have the same effect.

```
{
    "placeName": "localhost"
}
```

### Who do I talk to? ###

* [Kurtis Anatine](https://keybase.io/theguywho)