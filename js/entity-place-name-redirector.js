/*
    Kurtis's Not my place name, NOT ANYMORE ITS NOT script thing

    This script is released under the Awe Yeah Do Me Daddy licence agreement (AYDMD) basically this is just CC0, do whatever etc etc.

    If you have any questions get in contact with me at http://keybase.io/theguywho 
    If they can't back with a hash you better dash!

*/
(function() {
    // Change this to your places name. 
    this.enterEntity = function(entityID) {
        try {
            var placeName = JSON.parse(Entities.getEntityProperties(entityID).userData).placeName;
            // just going to lowercase it to cover if its typed weird or something #noideaifthatsathing
            if (Window.location.hostname.toLowerCase() !== placeName.toLowerCase()) { 
                Window.location = "hifi://"+placeName;
            }
        } catch (e) {
            // Either the userdata does not exist or something funky is going on.
        }
    };
});